
""" Creador:  Fredys Marquez Duque"""

from tkinter import*
from time import*
import winsound
import VW as ER
import S_S 
import Creditos


S_S.Splash_Screen()


class Ventana_Simulacion(object):
  """Ventana_simulacion integra las instacias de las clases: Cinta_Entrada, Grafo, Palabra_Entrada, Pila"""
  

  def __init__(self):


    self.root       = Tk() # Ventana principal
    self.tipo_letra = "Arial black "
    self.color      = "black"
    self.colorEtiquetas = "white" 
    
    self.root.configure(bg=self.color)
    self.root.geometry('800x600+290+50')
    self.root.overrideredirect(1)
    self.root.attributes("-alpha",0.9)
    self.Inicio          = PhotoImage(file="imagenes/estado_inicial.png")
    self.palabra    = StringVar()

    self.Entrada    = Entry(self.root, textvariable = self.palabra,width=26,relief="solid").place(x=170,y=123)
    
       
    self.cabeza     =  Button(self.root, text = "^" ,font =("Times new roman", 10),bg="red")  
    self.cabeza.place(x=10,y=230)     
    self.cabeza.place_forget()
    
    self.Ingresar   = Button(self.root, text="Ingresar",font =("Times new roman", 10),activebackground="black",fg="white",bg="brown",command=self.Ingresar_Cinta).place(x=370,y=120)
    self.Salir      = Button(self.root, text="Salir",font =("Times new roman", 10),bg="red",fg="white",command = self.root.destroy).place(x=766,y=0) 


    
    self.l_inicio        = Label(self.root, image=self.Inicio).place(x=120,y=300)         
    self.label_Expresion = Label(self.root, text="Ingrese la palabra",font =("Arial black", 10),bg=self.color ,fg=self.colorEtiquetas).place(x=25,y=120)  
    self.label_Cinta     = Label(self.root, text="Cinta entrada",font =("Arial black", 10), bg=self.color,fg=self.colorEtiquetas).place(x=120,y=170)
    self.label_titulo    = Label(self.root, text="AUTOMATA DE PILA\n(palabras impares)",font =("Arial black", 14),bg=self.color ,fg=self.colorEtiquetas).place(x=305,y=10) 
    self.label_pila      = Label(self.root, text="Pila",font =("Arial black", 12),bg=self.color,fg=self.colorEtiquetas).place(x=718,y=560)
    self.Nuevo           = Button(self.root, text="Nuevo",font =("Times new roman", 10),bg="green",fg=self.colorEtiquetas,command = self.Nuevo).place(x=0,y=0) 


    self.root.mainloop()
    Creditos.Creditos()


  def Nuevo(self):
      
      borrarC = Label(self.root,width=150,height=2,bg=self.color,fg=self.colorEtiquetas).place(x=10,y=200)
      self.cabeza.place_forget()  
      borrarP = Label(self.root,width=2,height=29,bg=self.color,fg=self.colorEtiquetas).place(x=724,y=120) 

  def Ingresar_Cinta(self):

      ObjetoC = Cinta_Entrada(self.root,self.palabra.get())    
      ObjetoC.Pintar_Cinta()
      self.Simulacion_Automata_de_Pila()


  def Simulacion_Automata_de_Pila(self):    


      ObjetoG = Grafo(self.root)
      ObjetoP = Pila(self.root,self.palabra.get())
      ObjetoC = Cinta_Entrada(self.root,self.palabra.get())      
      
      posicion=0
      decremento=-1
      palindroma=True
      lista_palindromo = list(self.palabra.get())

  
      if len(lista_palindromo) > 1 and len(lista_palindromo)%2!=0:

          tamaño = (len(lista_palindromo)-1)/2  
          lista_pila = lista_palindromo[0:int(tamaño)]

          while posicion <= len(lista_palindromo)-1:
      

              if posicion==0:
                 ObjetoG.Transiciones(0)
                 self.root.update()
         
              ObjetoC.Cabezal(self.cabeza,posicion)     
              winsound.PlaySound('sonidos/pin.wav',winsound.SND_FILENAME)

              if posicion < tamaño:

                  ObjetoG.Estados(0)
                  p = Button(self.root,text=lista_palindromo[posicion],bg="white",relief="flat",overrelief="raised").place(x=220,y=375)
                  ObjetoP.Apilar(posicion)         
                  decremento =  int((len(lista_pila)-1))
      
              if posicion == tamaño:
                  ObjetoG.Transiciones(1)
                  v = Button(self.root,text=lista_palindromo[posicion],bg="white",relief="flat",overrelief="raised").place(x=290,y=430)
                  sleep(1) 
     
              elif posicion>tamaño:
          
                  if lista_pila[decremento] == lista_palindromo[posicion]:
                      ObjetoG.Estados(1)
                      q = Button(self.root,text=lista_palindromo[posicion],bg="white",relief="flat",overrelief="raised").place(x=392,y=375)
                      ObjetoP.Desapilar(decremento)
                      decremento-=1
      
                  else:
                      palindroma=False
                      winsound.PlaySound('sonidos/no_palindroma.wav',winsound.SND_FILENAME)
                      break   

              self.root.update()         
              posicion+=1

  
          if palindroma == True:
             
             ObjetoG.Transiciones(2)
             sleep(1)
             self.root.update()
             sleep(2)
             posicion=0
             decremento=-1
             winsound.PlaySound('sonidos/palindroma.wav',winsound.SND_FILENAME)
      sleep(2)       
      self.l_inicio        = Label(self.root, image=self.Inicio).place(x=120,y=300)

   

class Palabra_Entrada(object):
  """Palabra_Entrada recibe la palabra ingresada, valida si es impar y si pertenece al alfabeto establecido([a-zA-Z]+)"""

  def __init__(self):
       pass

  
  def __init__(self,palabra):

          self.SetPalabra(palabra)
          self.lista_letras = list(self.GetPalabra())

  def GetPalabra(self):
          return self.palabra
    

  def GetLetras(self):

         if self.ValidarExpresion()==True:
            return self.lista_letras
         else:
            return False   
  
  def SetPalabra(self,palabra):
          self.palabra = palabra

  
    
  def  ValidarExpresion(self):

         if ER.ValidarExp(self.palabra) == 0:
            return True
         else:
            return False



class Nodo(object):
  """Nodo: Es la clase padre de las cuales heredan las clases: Cinta_Entrada, Grafo, Palabra_Entrada,Pila."""
 

  def __init__(self, nombre):
         
          self.SetNombre(nombre)
          
         
  def GetNombre(self):
          return self.nombre
    

  def SetNombre(self,nombre):
          self.nombre = nombre 

  

   
class Pila(Nodo):
  """Pila: Es una clase  que simula el apilado y desapilado de la palabra ingresada."""
  

  def __init__(self,palabra):
      ObjeN = Nodo.__init__(palabra)

  def __init__(self, root, palabra):

        self.root = root
        self.tipo_letra = "Arial black"
        self.palabra = palabra

        ObjetoPalabra = Palabra_Entrada(self.palabra) 
        
        self.lista_nodo = ObjetoPalabra.GetLetras()



    
  """Apilar: pintar botones con la letra que esta siendo validada, simulando la apilacion"""

  def Apilar(self,posicion):

        if self.ValidarExpresion():

          self.lista_nodo[posicion] = Button(self.root, text = self.lista_nodo[posicion],font =(self.tipo_letra, 10),width=1,height=1,bg="turquoise")
          self.lista_nodo[posicion].place(x=724,y=530-25*posicion)

	   
  """Desapilar: borrar botones con la letra que esta siendo validada, simulando la desapilacion"""

  def Desapilar(self,posicion): 

        if self.ValidarExpresion():

          self.lista_nodo[posicion] = Label(self.root,width=2,height=2,bg="black",fg="black")
          self.lista_nodo[posicion].place(x=724,y=530-25*posicion) 

  def  ValidarExpresion(self):

       if len(self.palabra)%2 != 0 and len(self.palabra)>1:
          return True
       else:
          return False    


class Cinta_Entrada(Nodo):
  """Cinta_Entrada: Es una clase que simula el recorrido de la palabra ingresada, validando si esta es palindroma."""
  
  
  def __init__(self,palabra):
      ObjeN = Nodo.__init__(palabra)


  def __init__(self,root,palabra):
     
        self.root = root
        self.tipo_letra = "Time new roman"
        self.color      = "turquoise"
        self.palabra = palabra

        self.ObjetoPalabra = Palabra_Entrada(self.palabra) 
        
        self.lista_nodo = self.ObjetoPalabra.GetLetras()



           
  """Pintar_Cinta, pintar la cinta con botones""" 
  def Pintar_Cinta(self):
        
      separacion       = 16  
      posicion_Botones = 0
        
      if self.ObjetoPalabra.GetLetras()!=False and self.ValidarExpresion():
        
        for letra in range(len(self.lista_nodo)):
 
            self.lista_nodo[letra] = Button(self.root, text = self.lista_nodo[letra] ,font =(self.tipo_letra, 10),width=1,height=1, overrelief="raised",relief="raised",activebackground="#F50743",bg=self.color).place(x=10+posicion_Botones,y=200)
            posicion_Botones += separacion

  """Cabezal, pintar el cabezal en una posicion especifica(posicion) """ 
  def Cabezal(self,cabeza,posicion):
       
      contador=0

      if self.ObjetoPalabra.GetLetras()!=False: 
         for x in range(len(self.lista_nodo)):
             if contador == posicion: 
                cabeza.place(x=10+contador*16,y=230)
             contador+=1

  """Mostrar_Cabezal, simular el movimiento por toda la cinta de entrada""" 
  
  def  ValidarExpresion(self):

       if len(self.palabra)%2 != 0 and len(self.palabra)>1:
          return True
       else:
          return False
  


class Grafo(Nodo):
  """Grafo: Es una clase  que simula las transiciones y estados en que se encentra la palabra ingresada."""

  def __init__(self,palabra):
      ObjeN = Nodo.__init__(palabra)

  
  def __init__(self,root):
      
      self.root = root
      self.flecha1    = PhotoImage(file="imagenes/flecha1.png")
      self.flecha2    = PhotoImage(file="imagenes/flecha2.png")
      self.flecha3    = PhotoImage(file="imagenes/flecha3.png")

      self.estado1    = PhotoImage(file="imagenes/estado1.png")    
      self.estado2    = PhotoImage(file="imagenes/estado2.png")    
      self.estado3    = PhotoImage(file="imagenes/estado3.png")
      
    
      
  """Transiciones: pintar botones con la letra que esta siendo validada"""  
  def Transiciones(self,transicion):

      if transicion ==  0:
         
         Transicion1 = Label(self.root, image=self.flecha1).place(x=120,y=300)

      elif transicion == 1:
         
         Transicion2 = Label(self.root, image=self.flecha2).place(x=120,y=300) 
      
      elif transicion == 2:

         Transicion3 = Label(self.root, image=self.flecha3).place(x=120,y=300) 
      

  """Estados: insertar imagenes simulando el recorrido del Grafo"""
  def Estados(self,estado):

      if estado == 0: 
      
          Estado1 =  Label(self.root, image=self.estado1).place(x=120,y=300) 
      
      elif estado == 1:
      
          Estado2 =  Label(self.root, image=self.estado2).place(x=120,y=300) 
      
      elif estado == 2:

           Estado3 =  Label(self.root, image=self.estado3).place(x=120,y=300) 
      



if __name__ == '__main__':
  
  v = Ventana_Simulacion()